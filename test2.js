/** @format */

// forEach() & map
// let n = [3, 4, 5, 6, 7, 8];

// n.forEach(function (e) {
//   console.log(e, "foreach");
// });
// n.map(function (e) {
//   console.log(e, "map");
// });
// untuk map kita harus mengubah objek menjadi array

// filter & find
// let n = [3, 4, 5, 6, 7, 8];

// n.filter(function (e) {
//   console.log(e > 5);
// });
// n.find(function (e) {
//   console.log(e == 5);
// });

// // spread operator
// const a = [0, 1, 2];
// const b = [3, 4, 5];
// c = [...a, ...b];
// console.log(c);
// // concrat
// const a = [0, 1, 2];
// const b = [3, 4, 5];
// c = [...a, ...b];
// c.concat;

// tugas menambahkan dan mengurutkan data
// const fruits = ["sleep", "work", "exercise"];
// fruits.sort();
// fruits.splice(0, 0, "eat");
// console.log(fruits);

// tugas jumlah a
// var arr1 = [3, "a", "a", "a", 2, 3, "a", 3, "a", 2, 4, 9, 3];
// var mf = 1;
// var m = 0;
// var item;
// for (var i = 0; i < arr1.length; i++) {
//   for (var j = i; j < arr1.length; j++) {
//     if (arr1[i] == arr1[j]) m++;
//     if (mf < m) {
//       mf = m;
//       item = arr1[i];
//     }
//   }
//   m = 0;
// }
// console.log(item + " ( " + mf + " times ) ");

// reduce
// math floor :
// let arr = [15.5, 2.3, 1.1, 4.7];

// function getSum(total, num) {
//   return total + Math.round(num);
// }

// console.log(arr.reduce(getSum, 0));

// async : sesuai dengan urutan, sync : bersama
// parameter adalah value yg diterima

// tugas
// valueElemen([1, 2, 3, 4, 5, 6, 7], 2); //output 2,4,6

// function valueElemen(arr, n) {
//   const result = [];
//   for (let i = n - 1; i < arr.length; i += n) {
//     result.push(arr[i]);
//   }
//   return result;
// }

// const inputArray = [1, 2, 3, 4, 5, 6, 7];
// const outputArray = valueElemen(inputArray, 2);
// console.log(outputArray);

// tugas no.1
// function pickerText(arr, i) {
//   if (i < 0 || i >= arr.length) {
//     return "throw error: Invalid weekday number";
//   } else {
//     const ChoseText = arr[i];
//     return ChoseText;
//   }
// }

// const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
// console.log(pickerText(weekdays, 0));
// console.log(pickerText(weekdays, 3));
// console.log(pickerText(weekdays, 5));

// tugas no.2
// function birthday(s, d, m) {
//   let count = 0;
//   for (let i = 0; i <= s.length - m; i++) {
//     let sum = 0;
//     for (let j = 0; j < m; j++) {
//       sum += s[i + j];
//     }
//     if (sum === d) {
//       count++;
//     }
//   }
//   return count;
// }

// console.log(birthday([2, 2, 1, 3, 2], 4, 2)); // Output: 2
// console.log(birthday([1, 1, 1, 1, 1], 3, 2));
// console.log(birthday([4], 4, 1));

// atau
function birthday(s, d, m) {
  let su = 0,
    c = 0;
  for (let i = 0; i <= s.length - m; ++i) {
    su += s[i];

    for (let j = i + m; j-- > i + 1; ) {
      su += s[j];
    }
    if (su === d) ++c;
    su = 0;
  }

  return c;
}

console.log(birthday([2, 2, 1, 3, 2], 4, 2)); // Output: 2
console.log(birthday([1, 1, 1, 1, 1], 3, 2)); // Output: 0
console.log(birthday([4], 4, 1)); // Output: 1
